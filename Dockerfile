FROM microsoft/dotnet:sdk AS build-env
WORKDIR /app

COPY . .
RUN dotnet restore
WORKDIR /app/src/DataPump.Api
RUN dotnet publish -c Release -o out

FROM microsoft/dotnet:aspnetcore-runtime
WORKDIR /app
COPY --from=build-env /app/src/DataPump.Api/out .
ENTRYPOINT ["dotnet", "DataPump.Api.dll"]
