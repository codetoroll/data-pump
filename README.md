# DataPump

## Подготовка

Необходимо установить API key для Github через Enviroment variables как GITHUB_KEY

## API

### Получение списка коммитов

/api/repo/{url}/commits/

Где url - url репозитория в URL encoding

```http
http://localhost:5000/api/repo/https%3A%2F%2Fbitbucket.org%2Fcodetoroll%2Fdata-pump/commits
```

### Получение списка коммитов с ветки

/api/repo/{url}/commits/{branch}

Где url - url репозитория в URL encoding, branch - название ветки в URL encoding

```http
http://localhost:5000/api/repo/https%3A%2F%2Fbitbucket.org%2Fcodetoroll%2Fdata-pump/commits/features%2Fdocker
```

### Получение коммита по id

/api/repo/{url}/commit/{id}

Где url - url репозитория в URL encoding, id - sha коммита(для Git репозитория)

```http
http://localhost:5000/api/repo/https%3A%2F%2Fbitbucket.org%2Fcodetoroll%2Fdata-pump/commit/a9db280
```


### Получение списка веток

/api/repo/{url}/branches

Где url - url репозитория в URL encoding

```http
http://localhost:5000/api/repo/https%3A%2F%2Fbitbucket.org%2Fcodetoroll%2Fdata-pump/branches
```

## Docker

### Сборка

```bash
docker build -t data-pump .
```

### Запуск
```bash
docker run -d -p 5000:80 --name data-pump-instance data-pump
```
