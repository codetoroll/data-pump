using System;
using System.Collections;
using System.Collections.Generic;
using DataPump.Core.Entity;
using DataPump.Core.Gateway;
using DataPump.Data.Database;
using FluentAssertions;
using Xbehave;
using Xunit;

// ReSharper disable ImplicitlyCapturedClosure

namespace DataPump.Data.Test
{
    public class TarantoolBranchesDatabaseTest
    {
        private IBranchesDatabase _branchesDatabase;

        private IEnumerable<BranchEntity> _listOfBranches = new[]
            {new BranchEntity {Name = "master"}, new BranchEntity {Name = "dev"}};

        private RepositoryLinkEntity testLink = new RepositoryLinkEntity("https://github.com/zerotwoone/test");

        public TarantoolBranchesDatabaseTest()
        {
            _branchesDatabase = new TarantoolBranchesDatabase();
        }

        [Scenario]
        public void StoreBranchesWithNullLink(RepositoryLinkEntity link, IEnumerable<BranchEntity> branches,
            Exception e)
        {
            "Given the null link"
                .x(() => link = null);

            "Given the branches collection"
                .x(() => branches = _listOfBranches);

            "When store branches"
                .x(async () => e = await Record.ExceptionAsync(() => _branchesDatabase.Store(link, branches)));

            "Then a ArgumentNullException is thrown"
                .x(() => e.Should().BeOfType<ArgumentNullException>());
        }

        [Scenario]
        public void StoreBranchesWithNullList(RepositoryLinkEntity link, IEnumerable<BranchEntity> branches,
            Exception e)
        {
            "Given the test link"
                .x(() => link = testLink);

            "Given the null branches collection"
                .x(() => branches = null);

            "When store branches"
                .x(async () => e = await Record.ExceptionAsync(() => _branchesDatabase.Store(link, branches)));

            "Then a ArgumentNullException is thrown"
                .x(() => e.Should().BeOfType<ArgumentNullException>());
        }

        [Scenario(Skip = "Нужно сконфигурировать Tarantool")]
        public void StoreBranches(RepositoryLinkEntity link, IEnumerable<BranchEntity> branches)
        {
            "Given the test link"
                .x(() => link = testLink);

            "Given the null branches collection"
                .x(() => branches = _listOfBranches);

            "When store branches"
                .x(async () => await _branchesDatabase.Store(link, branches));
        }
    }
}