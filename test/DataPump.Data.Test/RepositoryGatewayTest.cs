using System;
using System.Collections.Generic;
using DataPump.Core.Entity;
using DataPump.Core.Exception;
using DataPump.Core.Gateway;
using DataPump.Data.Repository.BitBucket;
using DataPump.Data.Repository.Github;
using DataPump.Data.Repository.Gitlab;
using FluentAssertions;
using Xbehave;
using Xunit;

// ReSharper disable ImplicitlyCapturedClosure

namespace DataPump.Data.Test
{
    public class RepositoryGatewayTest
    {
        public interface IRepositoryTestDataProvider
        {
            IEnumerable<BranchEntity> ExpectedBranches { get; }
            IEnumerable<CommitEntity> ExpectedMasterCommits { get; }

            CommitEntity ExpectedCommit { get; }

            IEnumerable<CommitEntity> ExpectedDevCommits { get; }
            string ValidDevBranch { get; }
            string ValidCommitId { get; }
            RepositoryLinkEntity TestRepoLink { get; }
            RepositoryLinkEntity NotExistsRepoLink { get; }
        }

        private class BitBucketTestDataProvider : IRepositoryTestDataProvider
        {
            public IEnumerable<BranchEntity> ExpectedBranches => new[]
            {
                new BranchEntity {Name = "master"}, new BranchEntity {Name = "dev"}
            };

            public IEnumerable<CommitEntity> ExpectedMasterCommits => new[]
            {
                new CommitEntity
                {
                    Hash = "47ade474584fc4ad030dfca5e17aede74d9ff04b",
                    Author = "Mazunin Konstantin <mazuninky@gmail.com>",
                    Message = "Test commit\n",
                    Date = DateTime.Parse("2018-10-30T11:36:19+00:00")
                },
                new CommitEntity
                {
                    Hash = "3b443783cae6cb02b6d6342f6c77797b480d3b42",
                    Author = "Mazunin Konstantin <mazunin_konstantin@niuitmo.ru>",
                    Message = "Initial commit",
                    Date = DateTime.Parse("2018-10-30T11:33:06+00:00")
                }
            };

            public IEnumerable<CommitEntity> ExpectedDevCommits => new[]
            {
                new CommitEntity
                {
                    Hash = "6c02bfbde433e20a12a2bdc88e551442de99cfd5",
                    Author = "Mazunin Konstantin <mazuninky@gmail.com>",
                    Message = "Dev commit\n",
                    Date = DateTime.Parse("2018-10-30T11:36:49+00:00")
                },
                new CommitEntity
                {
                    Hash = "47ade474584fc4ad030dfca5e17aede74d9ff04b",
                    Author = "Mazunin Konstantin <mazuninky@gmail.com>",
                    Message = "Test commit\n",
                    Date = DateTime.Parse("2018-10-30T11:36:19+00:00")
                },
                new CommitEntity
                {
                    Hash = "3b443783cae6cb02b6d6342f6c77797b480d3b42",
                    Author = "Mazunin Konstantin <mazunin_konstantin@niuitmo.ru>",
                    Message = "Initial commit",
                    Date = DateTime.Parse("2018-10-30T11:33:06+00:00")
                }
            };

            public string ValidDevBranch => "dev";

            public string ValidCommitId => "3b443783cae6cb02b6d6342f6c77797b480d3b42";

            public CommitEntity ExpectedCommit => new CommitEntity
            {
                Hash = "3b443783cae6cb02b6d6342f6c77797b480d3b42",
                Author = "Mazunin Konstantin <mazunin_konstantin@niuitmo.ru>",
                Message = "Initial commit",
                Date = DateTime.Parse("2018-10-30T11:33:06+00:00")
            };

            public RepositoryLinkEntity TestRepoLink =>
                new RepositoryLinkEntity("https://bitbucket.org/codetoroll/test/");

            public RepositoryLinkEntity NotExistsRepoLink =>
                new RepositoryLinkEntity("https://bitbucket.org/axzc/axzc/");
        }

        private class GithubTestDataProvider : IRepositoryTestDataProvider
        {
            public IEnumerable<BranchEntity> ExpectedBranches => new[]
            {
                new BranchEntity {Name = "master"}, new BranchEntity {Name = "dev"}
            };

            public IEnumerable<CommitEntity> ExpectedMasterCommits => new[]
            {
                new CommitEntity
                {
                    Hash = "633eb70da686b0e8b604430817274527164aaabb",
                    Author = "Mazunin Konstantin mazuninky@gmail.com",
                    Message = "Create Test.test",
                    Date = DateTime.Parse("2018-10-30T11:28:25")
                },
                new CommitEntity
                {
                    Hash = "158b2df375c371fb09737d1b2620710f7ee914b7",
                    Author = "Mazunin Konstantin mazuninky@gmail.com",
                    Message = "Initial commit",
                    Date = DateTime.Parse("2018-10-30T11:28:09")
                }
            };

            public IEnumerable<CommitEntity> ExpectedDevCommits => new[]
            {
                new CommitEntity
                {
                    Hash = "e741a071bf66c68364c8142ec6965045cad4656a",
                    Author = "Mazunin Konstantin mazuninky@gmail.com",
                    Message = "Add dev commit",
                    Date = DateTime.Parse("2018-11-01T11:46:38")
                },
                new CommitEntity
                {
                    Hash = "633eb70da686b0e8b604430817274527164aaabb",
                    Author = "Mazunin Konstantin mazuninky@gmail.com",
                    Message = "Create Test.test",
                    Date = DateTime.Parse("2018-10-30T11:28:25")
                },
                new CommitEntity
                {
                    Hash = "158b2df375c371fb09737d1b2620710f7ee914b7",
                    Author = "Mazunin Konstantin mazuninky@gmail.com",
                    Message = "Initial commit",
                    Date = DateTime.Parse("2018-10-30T11:28:09")
                }
            };


            public CommitEntity ExpectedCommit => new CommitEntity
            {
                Hash = "158b2df375c371fb09737d1b2620710f7ee914b7",
                Author = "Mazunin Konstantin mazuninky@gmail.com",
                Message = "Initial commit",
                Date = DateTime.Parse("2018-10-30T11:28:09")
            };

            public string ValidDevBranch => "dev";

            public string ValidCommitId => "158b2df375c371fb09737d1b2620710f7ee914b7";

            public RepositoryLinkEntity TestRepoLink =>
                new RepositoryLinkEntity("https://github.com/zerotwoone/test/");

            public RepositoryLinkEntity NotExistsRepoLink =>
                new RepositoryLinkEntity("https://github.com/axzc/axzc/");
        }

        private class GitlabTestDataProvider : IRepositoryTestDataProvider
        {
            public IEnumerable<BranchEntity> ExpectedBranches => new[]
            {
                new BranchEntity {Name = "master"}, new BranchEntity {Name = "dev"}
            };

            public IEnumerable<CommitEntity> ExpectedMasterCommits => new[]
            {
                new CommitEntity
                {
                    Hash = "818614ae0e80ec83b122acab0f11867c0cde2863",
                    Author = "Mazunin Konstantin mazuninky@gmail.com",
                    Message = "Initial commit",
                    Date = DateTime.Parse("2018-11-01T11:21:37.000")
                }
            };

            public IEnumerable<CommitEntity> ExpectedDevCommits => new[]
            {
                new CommitEntity
                {
                    Hash = "0596aa8356116aebd11f6b79f28b2f33b9815d11",
                    Author = "Mazunin Konstantin mazuninky@gmail.com",
                    Message = "Add new file",
                    Date = DateTime.Parse("2018-11-01T11:22:26.000")
                },
                new CommitEntity
                {
                    Hash = "818614ae0e80ec83b122acab0f11867c0cde2863",
                    Author = "Mazunin Konstantin mazuninky@gmail.com",
                    Message = "Initial commit",
                    Date = DateTime.Parse("2018-11-01T11:21:37.000")
                }
            };

            public CommitEntity ExpectedCommit => new CommitEntity
            {
                Hash = "818614ae0e80ec83b122acab0f11867c0cde2863",
                Author = "Mazunin Konstantin mazuninky@gmail.com",
                Message = "Initial commit",
                Date = DateTime.Parse("2018-11-01T11:21:37.000")
            };

            public string ValidDevBranch => "dev";

            public string ValidCommitId => "818614ae0e80ec83b122acab0f11867c0cde2863";

            public RepositoryLinkEntity TestRepoLink => new RepositoryLinkEntity("https://gitlab.com/zerotwoone/test");

            public RepositoryLinkEntity NotExistsRepoLink =>
                new RepositoryLinkEntity("https://gitlab.com/axzc/axzc/");
        }

        // ReSharper disable once MemberCanBePrivate.Global
        public static IEnumerable<object[]> GatewaysData =>
            new List<object[]>
            {
                new object[] {new BitBucketGateway(), new BitBucketTestDataProvider()},
                new object[] {new GithubGateway(), new GithubTestDataProvider()},
                new object[] {new GitlabGateway(), new GitlabTestDataProvider()}
            };

        private readonly RepositoryLinkEntity _undefinedServiceLink =
            new RepositoryLinkEntity("https://test.xyz/test/test");


        [Scenario]
        [MemberData(nameof(GatewaysData))]
        public void FetchingBranchesWithNullLink(IRepositoryGateway repositoryGateway,
            IRepositoryTestDataProvider dataProvider, RepositoryLinkEntity link, Exception e)
        {
            "Given the null link"
                .x(() => link = null);

            "When fetching list of branches"
                .x(async () => e = await Record.ExceptionAsync(() => repositoryGateway.FetchBranches(link)));

            "Then a ArgumentNullException is thrown"
                .x(() => e.Should().BeOfType<ArgumentNullException>());
        }

        [Scenario]
        [MemberData(nameof(GatewaysData))]
        public void FetchingBranchesFromNotMatchingService(IRepositoryGateway repositoryGateway,
            IRepositoryTestDataProvider dataProvider, RepositoryLinkEntity link, Exception e)
        {
            "Given the not matching link"
                .x(() => link = _undefinedServiceLink);

            "When fetching list of branches"
                .x(async () => e = await Record.ExceptionAsync(() => repositoryGateway.FetchBranches(link)));

            "Then a ArgumentException is thrown"
                .x(() => e.Should().BeOfType<ArgumentException>());
        }


        [Scenario]
        [MemberData(nameof(GatewaysData))]
        public void FetchingBranchesWithNotExistsRepo(IRepositoryGateway repositoryGateway,
            IRepositoryTestDataProvider dataProvider, RepositoryLinkEntity link, Exception e)
        {
            "Given the not exists repo"
                .x(() => link = dataProvider.NotExistsRepoLink);

            "When fetching list of branches"
                .x(async () => e = await Record.ExceptionAsync(() => repositoryGateway.FetchBranches(link)));

            "Then a NotExistsRepoException is thrown"
                .x(() => e.Should().BeOfType<RepositoryException.NotExistsRepoException>());
        }

        [Scenario]
        [MemberData(nameof(GatewaysData))]
        public void FetchingBranchesFromTestRepo(IRepositoryGateway repositoryGateway,
            IRepositoryTestDataProvider dataProvider, RepositoryLinkEntity link, IEnumerable<BranchEntity> branches)
        {
            "Given the test repo link"
                .x(() => link = dataProvider.TestRepoLink);

            "When fetching list of branches"
                .x(async () => branches = await repositoryGateway.FetchBranches(link));

            "Then a result is expected"
                .x(() => branches.Should().BeEquivalentTo(dataProvider.ExpectedBranches));
        }


        [Scenario]
        [MemberData(nameof(GatewaysData))]
        public void FetchingCommitsByBranchWithNullLink(IRepositoryGateway repositoryGateway,
            IRepositoryTestDataProvider dataProvider,
            RepositoryLinkEntity link, string branch, Exception e)
        {
            "Given the null link"
                .x(() => link = null);

            "Given the valid branch"
                .x(() => branch = dataProvider.ValidDevBranch);

            "When fetching list of commits by branch"
                .x(async () => e = await Record.ExceptionAsync(() => repositoryGateway.FetchCommits(link, branch)));

            "Then a ArgumentNullException is thrown"
                .x(() => e.Should().BeOfType<ArgumentNullException>());
        }

        [Scenario]
        [MemberData(nameof(GatewaysData))]
        public void FetchingCommitsByBranchWithNullBranch(IRepositoryGateway repositoryGateway,
            IRepositoryTestDataProvider dataProvider, RepositoryLinkEntity link, string branch, Exception e)
        {
            "Given the test repo link"
                .x(() => link = dataProvider.TestRepoLink);

            "Given the null branch"
                .x(() => branch = null);

            "When fetching list of commits by branch"
                .x(async () => e = await Record.ExceptionAsync(() => repositoryGateway.FetchCommits(link, branch)));

            "Then a ArgumentNullException is thrown"
                .x(() => e.Should().BeOfType<ArgumentNullException>());
        }

        [Scenario]
        [MemberData(nameof(GatewaysData))]
        public void FetchingCommitsByBranchFromNotMatchingService(IRepositoryGateway repositoryGateway,
            IRepositoryTestDataProvider dataProvider, RepositoryLinkEntity link, string branch,
            Exception e)
        {
            "Given the not matching link"
                .x(() => link = _undefinedServiceLink);

            "Given the valid branch"
                .x(() => branch = dataProvider.ValidDevBranch);

            "When fetching list of commits by branch"
                .x(async () => e = await Record.ExceptionAsync(() => repositoryGateway.FetchCommits(link, branch)));

            "Then a ArgumentException is thrown"
                .x(() => e.Should().BeOfType<ArgumentException>());
        }

        [Scenario]
        [MemberData(nameof(GatewaysData))]
        public void FetchingCommitsByBranchFromTestRepo(IRepositoryGateway repositoryGateway,
            IRepositoryTestDataProvider dataProvider, RepositoryLinkEntity link, string branch,
            IEnumerable<CommitEntity> commits)
        {
            "Given the test repo link"
                .x(() => link = dataProvider.TestRepoLink);

            "Given the dev branch"
                .x(() => branch = dataProvider.ValidDevBranch);

            "When fetching list of commits"
                .x(async () => commits = await repositoryGateway.FetchCommits(link, branch));

            "Then a result is expected"
                .x(() => commits.Should().BeEquivalentTo(dataProvider.ExpectedDevCommits));
        }

        [Scenario]
        [MemberData(nameof(GatewaysData))]
        public void FetchingCommitsWithNullLink(IRepositoryGateway repositoryGateway,
            IRepositoryTestDataProvider dataProvider, RepositoryLinkEntity link, Exception e)
        {
            "Given the null link"
                .x(() => link = null);

            "When fetching list of commits"
                .x(async () => e = await Record.ExceptionAsync(() => repositoryGateway.FetchCommits(link)));

            "Then a ArgumentNullException is thrown"
                .x(() => e.Should().BeOfType<ArgumentNullException>());
        }

        [Scenario]
        [MemberData(nameof(GatewaysData))]
        public void FetchingCommitsFromNotMachingService(IRepositoryGateway repositoryGateway,
            IRepositoryTestDataProvider dataProvider, RepositoryLinkEntity link, Exception e)
        {
            "Given the not matching link"
                .x(() => link = _undefinedServiceLink);

            "When fetching list of commits"
                .x(async () => e = await Record.ExceptionAsync(() => repositoryGateway.FetchCommits(link)));

            "Then a ArgumentException is thrown"
                .x(() => e.Should().BeOfType<ArgumentException>());
        }


        [Scenario]
        [MemberData(nameof(GatewaysData))]
        public void FetchingCommitsFromTestRepo(IRepositoryGateway repositoryGateway,
            IRepositoryTestDataProvider dataProvider, RepositoryLinkEntity link, IEnumerable<CommitEntity> commits)
        {
            "Given the test repo link"
                .x(() => link = dataProvider.TestRepoLink);

            "When fetching list of commits"
                .x(async () => commits = await repositoryGateway.FetchCommits(link));

            "Then a result is expected"
                .x(() => commits.Should().BeEquivalentTo(dataProvider.ExpectedMasterCommits));
        }


        [Scenario]
        [MemberData(nameof(GatewaysData))]
        public void FetchingCommitByIdWithNullLink(IRepositoryGateway repositoryGateway,
            IRepositoryTestDataProvider dataProvider, RepositoryLinkEntity link, string id, Exception e)
        {
            "Given the null link"
                .x(() => link = null);

            "Given the valid id"
                .x(() => id = dataProvider.ValidCommitId);

            "When fetching commit by id"
                .x(async () => e = await Record.ExceptionAsync(() => repositoryGateway.FetchCommit(link, id)));

            "Then a ArgumentNullException is thrown"
                .x(() => e.Should().BeOfType<ArgumentNullException>());
        }

        [Scenario]
        [MemberData(nameof(GatewaysData))]
        public void FetchingCommitByIdWithNullId(IRepositoryGateway repositoryGateway,
            IRepositoryTestDataProvider dataProvider, RepositoryLinkEntity link, string id, Exception e)
        {
            "Given the test repo link"
                .x(() => link = dataProvider.TestRepoLink);

            "Given the null id"
                .x(() => id = null);

            "When fetching commit by id"
                .x(async () => e = await Record.ExceptionAsync(() => repositoryGateway.FetchCommit(link, id)));

            "Then a ArgumentNullException is thrown"
                .x(() => e.Should().BeOfType<ArgumentNullException>());
        }

        [Scenario]
        [MemberData(nameof(GatewaysData))]
        public void FetchingCommitByIdFromNotMatchingService(IRepositoryGateway repositoryGateway,
            IRepositoryTestDataProvider dataProvider, RepositoryLinkEntity link, string id, Exception e)
        {
            "Given the not matching service link"
                .x(() => link = _undefinedServiceLink);

            "Given the null id"
                .x(() => id = dataProvider.ValidCommitId);

            "When fetching commit by id"
                .x(async () => e = await Record.ExceptionAsync(() => repositoryGateway.FetchCommit(link, id)));

            "Then a ArgumentException is thrown"
                .x(() => e.Should().BeOfType<ArgumentException>());
        }

        [Scenario]
        [MemberData(nameof(GatewaysData))]
        public void FetchingCommitByIdFromTestRepo(IRepositoryGateway repositoryGateway,
            IRepositoryTestDataProvider dataProvider, RepositoryLinkEntity link, string id, CommitEntity commit)
        {
            "Given the test repo link"
                .x(() => link = dataProvider.TestRepoLink);

            "Given the valid id"
                .x(() => id = dataProvider.ValidCommitId);

            "When fetching commit by id"
                .x(async () => commit = await repositoryGateway.FetchCommit(link, id));

            "Then a result is expected"
                .x(() => commit.Should().BeEquivalentTo(dataProvider.ExpectedCommit));
        }
    }
}