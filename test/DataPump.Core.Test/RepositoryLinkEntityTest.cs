using System;
using DataPump.Core.Entity;
using FluentAssertions;
using Xbehave;
using Xunit;
// ReSharper disable ImplicitlyCapturedClosure

namespace DataPump.Core.Test
{
    public class RepositoryLinkEntityTest
    {
        [Scenario]
        public void UsingNullUri(Uri uri, System.Exception e)
        {
            "Given the null uri"
                .x(() => uri = null);

            "When creating repository link"
                .x(() => e = Record.Exception(() => new RepositoryLinkEntity(uri)));

            "Then a ArgumentNullException is thrown"
                .x(() => e.Should().BeOfType<ArgumentNullException>());
        }

        [Scenario]
        public void UsingHttpUrl(Uri uri, RepositoryLinkEntity link)
        {
            "Given the http url"
                .x(() => uri = new Uri("http://github.com/zerotwoone/dsa/"));

            "When creating repository link"
                .x(() => link = new RepositoryLinkEntity(uri));

            "Then a protocol is https"
                .x(() => link.Protocol.Should().BeEquivalentTo(RepositoryLinkEntity.LinkProtocol.Http));
        }


        [Scenario]
        public void UsingHttpsUrl(Uri uri, RepositoryLinkEntity link)
        {
            "Given the http url"
                .x(() => uri = new Uri("https://github.com/zerotwoone/dsa/"));

            "When creating repository link"
                .x(() => link = new RepositoryLinkEntity(uri));

            "Then a protocol is https"
                .x(() => link.Protocol.Should().BeEquivalentTo(RepositoryLinkEntity.LinkProtocol.Https));
        }


        [Scenario]
        public void UsingBitBucketUri(Uri uri, RepositoryLinkEntity link)
        {
            const string username = "codetoroll";
            const string slug = "data-pump";

            "Given the BitBucket uri"
                .x(() => uri = new Uri($"https://bitbucket.org/{username}/{slug}/"));

            "When creating repository link"
                .x(() => link = new RepositoryLinkEntity(uri));

            "Then a protocol is https"
                .x(() => link.Protocol.Should().BeEquivalentTo(RepositoryLinkEntity.LinkProtocol.Https));

            "Then a service is BitBucket"
                .x(() => link.Service.Should().BeEquivalentTo(RepositoryLinkEntity.RepositoryService.BitBucket));

            $"Then a username is {username}"
                .x(() => link.UserName.Should().BeEquivalentTo(username));

            $"Then a slug is {slug}"
                .x(() => link.Slug.Should().BeEquivalentTo(slug));
        }

        [Scenario]
        public void UsingGithubUri(Uri uri, RepositoryLinkEntity link)
        {
            const string username = "zerotwoone";
            const string slug = "dsa";

            "Given the Github uri"
                .x(() => uri = new Uri($"https://github.com/{username}/{slug}/"));

            "When creating repository link"
                .x(() => link = new RepositoryLinkEntity(uri));

            "Then a protocol is https"
                .x(() => link.Protocol.Should().BeEquivalentTo(RepositoryLinkEntity.LinkProtocol.Https));

            "Then a service is Github"
                .x(() => link.Service.Should().BeEquivalentTo(RepositoryLinkEntity.RepositoryService.Github));

            $"Then a username is {username}"
                .x(() => link.UserName.Should().BeEquivalentTo(username));

            $"Then a slug is {slug}"
                .x(() => link.Slug.Should().BeEquivalentTo(slug));
        }

        [Scenario]
        public void UsingGitlabUri(Uri uri, RepositoryLinkEntity link)
        {
            const string username = "zerotwoone";
            const string slug = "android-boilerplate";

            "Given the Gitlab uri"
                .x(() => uri = new Uri($"https://gitlab.com/{username}/{slug}/"));

            "When creating repository link"
                .x(() => link = new RepositoryLinkEntity(uri));

            "Then a protocol is https"
                .x(() => link.Protocol.Should().BeEquivalentTo(RepositoryLinkEntity.LinkProtocol.Https));

            "Then a service is BitBucket"
                .x(() => link.Service.Should().BeEquivalentTo(RepositoryLinkEntity.RepositoryService.Gitlab));

            $"Then a username is {username}"
                .x(() => link.UserName.Should().BeEquivalentTo(username));

            $"Then a slug is {slug}"
                .x(() => link.Slug.Should().BeEquivalentTo(slug));
        }
    }
}