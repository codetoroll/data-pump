using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using FluentAssertions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Xbehave;
using Xunit;
// ReSharper disable ImplicitlyCapturedClosure

namespace DataPump.Api.Test
{
    public class BranchesControllerTests : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _factory;
        private readonly HttpClient _client;

        public BranchesControllerTests(WebApplicationFactory<Startup> factory)
        {
            _factory = factory;

            _client = _factory.CreateClient();
        }

        private const string TestRepoUrl = "https://bitbucket.org/codetoroll/test/";
        
        [Scenario(Skip = "Необходимо решить проблему с 404")]
        public void ListOfBranchesTest(HttpResponseMessage response)
        {   
            "Given the branches request"
                .x(async () =>
                    response = await _client.GetAsync($"api/repo/{HttpUtility.UrlEncode(TestRepoUrl)}/branches"));

            "Then get success response"
                .x(() => response.EnsureSuccessStatusCode());
        }
    }
}