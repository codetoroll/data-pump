using System.Net.Http;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc.Testing;
using Xbehave;
using Xunit;
// ReSharper disable ImplicitlyCapturedClosure

namespace DataPump.Api.Test
{
    public class TestController : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _factory;
        private readonly HttpClient _client;

        public TestController(WebApplicationFactory<Startup> factory)
        {
            _factory = factory;

            _client = _factory.CreateClient();
        }

        [Scenario]
        public void Test(HttpResponseMessage response, string content)
        {
            "When make branches request"
                .x(async () =>
                    response = await _client.GetAsync("api/repo/ping"));

            "Then get success response"
                .x(() => response.EnsureSuccessStatusCode());

            "When read string"
                .x(async () => content = await response.Content.ReadAsStringAsync());

            "Then get string 'Pong'"
                .x(async () => content.Should().BeEquivalentTo("Pong"));
        }
    }
}