using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataPump.Core.Entity;
using DataPump.Core.Gateway;
using DataPump.Core.UseCase;
using FluentAssertions;
using Moq;
using Xbehave;
using Xunit;

// ReSharper disable ImplicitlyCapturedClosure

namespace DataPump.Domain.Test
{
    public class FetchCommitByIdUseCaseTest
    {
        private readonly IFetchCommitByIdUseCase _fetchCommit;
        private readonly Mock<IRepositoryGateway> _repositoryGateway;

        private static readonly RepositoryLinkEntity ValidLink =
            new RepositoryLinkEntity("https://bitbucket.org/codetoroll/test/");

        private const string ValidId = "affafsf121";

        private static readonly CommitEntity ExpectedCommit = new CommitEntity
            {Author = "Test1", Message = "Test1"};

        public FetchCommitByIdUseCaseTest()
        {
            _repositoryGateway = new Mock<IRepositoryGateway>();
            _repositoryGateway.Setup(gateway => gateway.FetchCommit(ValidLink, ValidId))
                .ReturnsAsync(ExpectedCommit);
            _fetchCommit = new FetchCommitByIdUseCase(_repositoryGateway.Object);
        }

        [Scenario]
        public void UsingNullLink(RepositoryLinkEntity link, string id, Exception e)
        {
            "Given the null link"
                .x(() => link = null);

            "Given the valid id"
                .x(() => id = ValidId);

            "When fetching list of commits"
                .x(async () => e = await Record.ExceptionAsync(() => _fetchCommit.Fetch(link, id)));

            "Then a ArgumentNullException is thrown"
                .x(() => Assert.IsType<ArgumentNullException>(e));
        }

        [Scenario]
        public void UsingNullBranch(RepositoryLinkEntity link, string id, Exception e)
        {
            "Given the valid link"
                .x(() => link = ValidLink);

            "Given the null id"
                .x(() => id = null);

            "When fetching list of commits"
                .x(async () => e = await Record.ExceptionAsync(() => _fetchCommit.Fetch(link, id)));

            "Then a ArgumentNullException is thrown"
                .x(() => Assert.IsType<ArgumentNullException>(e));
        }

        [Scenario]
        public void ValidRequest(RepositoryLinkEntity link, string id, CommitEntity commit)
        {
            "Given the valid link"
                .x(() => link = ValidLink);

            "Given the valid branch"
                .x(() => id = ValidId);

            "When fetching list of commits"
                .x(async () => commit = await _fetchCommit.Fetch(link, id));

            "Then call FetchCommit method on stub"
                .x(() => _repositoryGateway.Verify(gateway => gateway.FetchCommit(link, id)));

            "Then get expected commit"
                .x(() => commit.Should().BeEquivalentTo(ExpectedCommit));
        }
    }
}