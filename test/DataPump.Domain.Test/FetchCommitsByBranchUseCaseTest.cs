using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataPump.Core.Entity;
using DataPump.Core.Gateway;
using DataPump.Core.UseCase;
using FluentAssertions;
using Moq;
using Xbehave;
using Xunit;

// ReSharper disable ImplicitlyCapturedClosure

namespace DataPump.Domain.Test
{
    public class FetchCommitsByBranchUseCaseTest
    {
        private readonly IFetchCommitsByBranchUseCase _fetchCommits;
        private readonly Mock<IRepositoryGateway> _repositoryGateway;

        private static readonly RepositoryLinkEntity ValidLink =
            new RepositoryLinkEntity("https://bitbucket.org/codetoroll/test/");

        private const string ValidBranch = "dev";

        private static readonly IEnumerable<CommitEntity> ExpectedCommits = new[]
        {
            new CommitEntity {Author = "Test1", Message = "Test1"},
            new CommitEntity {Author = "Test2", Message = "Test2"}
        };

        public FetchCommitsByBranchUseCaseTest()
        {
            _repositoryGateway = new Mock<IRepositoryGateway>();
            _repositoryGateway.Setup(gateway => gateway.FetchCommits(ValidLink, ValidBranch))
                .ReturnsAsync(ExpectedCommits);
            _fetchCommits = new FetchCommitsByBranchUseCase(_repositoryGateway.Object);
        }

        [Scenario]
        public void UsingNullLink(RepositoryLinkEntity link, string branch, Exception e)
        {
            "Given the null link"
                .x(() => link = null);

            "Given the valid branch"
                .x(() => branch = ValidBranch);

            "When fetching list of commits"
                .x(async () => e = await Record.ExceptionAsync(() => _fetchCommits.Fetch(link, branch)));

            "Then a ArgumentNullException is thrown"
                .x(() => Assert.IsType<ArgumentNullException>(e));
        }

        [Scenario]
        public void UsingNullBranch(RepositoryLinkEntity link, string branch, Exception e)
        {
            "Given the valid link"
                .x(() => link = ValidLink);

            "Given the null branch"
                .x(() => branch = null);

            "When fetching list of commits"
                .x(async () => e = await Record.ExceptionAsync(() => _fetchCommits.Fetch(link, branch)));

            "Then a ArgumentNullException is thrown"
                .x(() => Assert.IsType<ArgumentNullException>(e));
        }

        [Scenario]
        public void ValidRequest(RepositoryLinkEntity link, string branch, IEnumerable<CommitEntity> commits)
        {
            "Given the valid link"
                .x(() => link = ValidLink);

            "Given the valid branch"
                .x(() => branch = ValidBranch);

            "When fetching list of commits"
                .x(async () => commits = await _fetchCommits.Fetch(link, branch));

            "Then call FetchCommits method on stub"
                .x(() => _repositoryGateway.Verify(gateway => gateway.FetchCommits(link, branch)));

            "Then get expected commits"
                .x(() => commits.Should().BeEquivalentTo(ExpectedCommits));
        }
    }
}