using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataPump.Core.Entity;
using DataPump.Core.Gateway;
using DataPump.Core.UseCase;
using FluentAssertions;
using Moq;
using Xbehave;
using Xunit;

// ReSharper disable ImplicitlyCapturedClosure

namespace DataPump.Domain.Test
{
    public class FetchBranchesUseCaseTest
    {
        private readonly IFetchBranchesListUseCase _fetchBranches;
        private readonly Mock<IRepositoryGateway> _repositoryGateway;

        private static readonly RepositoryLinkEntity ValidLink =
            new RepositoryLinkEntity("https://bitbucket.org/codetoroll/test/");

        private static readonly IEnumerable<BranchEntity> ExpectedBranches = new[]
        {
            new BranchEntity {Name = "dev"}, new BranchEntity {Name = "master"}
        };

        public FetchBranchesUseCaseTest()
        {
            _repositoryGateway = new Mock<IRepositoryGateway>();
            _repositoryGateway.Setup(gateway => gateway.FetchBranches(ValidLink)).ReturnsAsync(ExpectedBranches);
            _fetchBranches = new FetchBranchesListUseCase(_repositoryGateway.Object);
        }

        [Scenario]
        public void UsingNullLink(RepositoryLinkEntity link, Exception e)
        {
            "Given the null link"
                .x(() => link = null);

            "When fetching list of branches"
                .x(async () => e = await Record.ExceptionAsync(() => _fetchBranches.Fetch(link)));

            "Then a ArgumentNullException is thrown"
                .x(() => Assert.IsType<ArgumentNullException>(e));
        }

        [Scenario]
        public void UsingValidLink(RepositoryLinkEntity link, IEnumerable<BranchEntity> commits)
        {
            "Given the valid link"
                .x(() => link = ValidLink);

            "When fetching list of branches"
                .x(async () => commits = await _fetchBranches.Fetch(link));

            "Then call FetcBranches method on stub"
                .x(() => _repositoryGateway.Verify(gateway => gateway.FetchBranches(link)));

            "Then get expected commits"
                .x(() => commits.Should().BeEquivalentTo(ExpectedBranches));
        }
    }
}