using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataPump.Core.Entity;
using DataPump.Core.Exception;
using DataPump.Core.Gateway;
using DataPump.Core.Sharp;
using DataPump.Core.UseCase;

namespace DataPump.Domain
{
    public sealed class FetchBranchesListUseCase : IFetchBranchesListUseCase
    {
        private readonly IRepositoryGateway _repositoryGateway;

        public FetchBranchesListUseCase(IRepositoryGateway repositoryGateway)
        {
            _repositoryGateway = repositoryGateway;
        }

        public async Task<IEnumerable<BranchEntity>> Fetch(RepositoryLinkEntity link)
        {
            link.ThrowIfNull();

            try
            {
                return await _repositoryGateway.FetchBranches(link);
            }
            catch (RepositoryException.NotExistsRepoException e)
            {
                throw new FetchBranchesListException.NotExistsRepoException();
            }
        }
    }
}