using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataPump.Core.Entity;
using DataPump.Core.Gateway;
using DataPump.Core.Sharp;
using DataPump.Core.UseCase;
using JetBrains.Annotations;

namespace DataPump.Domain
{
    public sealed class FetchCommitsUseCase : IFetchCommitsUseCase
    {
        private readonly IRepositoryGateway _repositoryGateway;

        public FetchCommitsUseCase(IRepositoryGateway repositoryGateway)
        {
            _repositoryGateway = repositoryGateway;
        }

        public Task<IEnumerable<CommitEntity>> Fetch(RepositoryLinkEntity link)
        {
            link.ThrowIfNull();

            return _repositoryGateway.FetchCommits(link);
        }
    }
}