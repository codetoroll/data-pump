using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataPump.Core.Entity;
using DataPump.Core.Gateway;
using DataPump.Core.Sharp;
using DataPump.Core.UseCase;

namespace DataPump.Domain
{
    public sealed class FetchCommitByIdUseCase : IFetchCommitByIdUseCase
    {
        private readonly IRepositoryGateway _repositoryGateway;

        public FetchCommitByIdUseCase(IRepositoryGateway repositoryGateway)
        {
            _repositoryGateway = repositoryGateway;
        }

        public Task<CommitEntity> Fetch(RepositoryLinkEntity link, string id)
        {
            link.ThrowIfNull();
            id.ThrowIfNull();

            return _repositoryGateway.FetchCommit(link, id);
        }
    }
}