using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataPump.Core.Entity;
using DataPump.Core.Gateway;
using DataPump.Core.Sharp;
using DataPump.Core.UseCase;

namespace DataPump.Domain
{
    public sealed class FetchCommitsByBranchUseCase : IFetchCommitsByBranchUseCase
    {
        private readonly IRepositoryGateway _repositoryGateway;

        public FetchCommitsByBranchUseCase(IRepositoryGateway repositoryGateway)
        {
            _repositoryGateway = repositoryGateway;
        }

        public Task<IEnumerable<CommitEntity>> Fetch(RepositoryLinkEntity link, string branch)
        {
            link.ThrowIfNull();
            branch.ThrowIfNull();

            return _repositoryGateway.FetchCommits(link, branch);
        }
    }
}