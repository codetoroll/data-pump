﻿using System.IO;
using DataPump.Core;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace DataPump.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            if (Directory.Exists(Constants.RepoPath))
                Directory.Delete(Constants.RepoPath, true);
            Directory.CreateDirectory(Constants.RepoPath);
            
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.AddEnvironmentVariables();
                })
                .UseStartup<Startup>();
    }
}