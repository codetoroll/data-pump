﻿using DataPump.Api.Controllers;
using DataPump.Core.Gateway;
using DataPump.Core.UseCase;
using DataPump.Data;
using DataPump.Data.Repository;
using DataPump.Data.Repository.BitBucket;
using DataPump.Data.Repository.Git;
using DataPump.Data.Repository.Github;
using DataPump.Data.Repository.Gitlab;
using DataPump.Domain;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DataPump.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSingleton<IGitGateway, GitGateway>()
                .AddSingleton<IBitBucketGateway, BitBucketGateway>()
                .AddSingleton<IGithubGateway, GithubGateway>()
                .AddSingleton<IGitlabGateway, GitlabGateway>()
                .AddSingleton<IRepositoryGateway, RepositoryFacade>();

            services.AddTransient<IFetchCommitsUseCase, FetchCommitsUseCase>()
                .AddTransient<IFetchCommitByIdUseCase, FetchCommitByIdUseCase>()
                .AddTransient<IFetchCommitsByBranchUseCase, FetchCommitsByBranchUseCase>()
                .AddTransient<IFetchBranchesListUseCase, FetchBranchesListUseCase>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            //app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}