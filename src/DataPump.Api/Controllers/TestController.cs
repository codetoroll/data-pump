using Microsoft.AspNetCore.Mvc;

namespace DataPump.Api.Controllers
{
    [Route("api/repo")]
    [ApiController]
    public class TestController  : ControllerBase
    {
      
        [HttpGet]
        public string Test()
        {
            return "Test";
        }
        
        [HttpGet("ping")]
        public string Ping()
        {
            return "Pong";
        }
    }
}