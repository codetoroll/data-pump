using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using DataPump.Core.Entity;
using DataPump.Core.Exception;
using DataPump.Core.UseCase;
using Microsoft.AspNetCore.Mvc;

namespace DataPump.Api.Controllers
{
    [Route("api/repo")]
    [ApiController]
    public class BranchesController : ControllerBase
    {
        private readonly IFetchBranchesListUseCase _fetchBranchesList;

        public BranchesController(IFetchBranchesListUseCase fetchBranchesList, IFetchCommitByIdUseCase fetchCommit)
        {
            _fetchBranchesList = fetchBranchesList;
        }

        private static RepositoryLinkEntity ParseUrl(string url)
        {
            return new RepositoryLinkEntity(new Uri(HttpUtility.UrlDecode(url)));
        }

        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [HttpGet("{url}/branches", Name = "FetchBranches")]
        public async Task<ActionResult<IEnumerable<BranchEntity>>> FetchBranches(string url)
        {
            RepositoryLinkEntity link;
            try
            {
                link = ParseUrl(url);
            }
            catch (UriFormatException e)
            {
                return BadRequest("Url parsing exception");
            }

            try
            {
                return Ok(await _fetchBranchesList.Fetch(link));
            }
            catch (FetchBranchesListException.NotExistsRepoException e)
            {
                return NotFound();
            }
        }
    }
}