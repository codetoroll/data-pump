using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using DataPump.Core.Entity;
using DataPump.Core.UseCase;
using Microsoft.AspNetCore.Mvc;

namespace DataPump.Api.Controllers
{
    [Route("api/repo")]
    [ApiController]
    public class CommitsController : ControllerBase
    {
        private readonly IFetchCommitsUseCase _fetchCommits;
        private readonly IFetchCommitByIdUseCase _fetchCommitById;
        private readonly IFetchCommitsByBranchUseCase _fetchCommitsByBranch;

        public CommitsController(IFetchCommitsUseCase fetchCommits, IFetchCommitByIdUseCase fetchCommitById,
            IFetchCommitsByBranchUseCase fetchCommitsByBranch)
        {
            _fetchCommits = fetchCommits;
            _fetchCommitById = fetchCommitById;
            _fetchCommitsByBranch = fetchCommitsByBranch;
        }

        private static RepositoryLinkEntity ParseUrl(string url)
        {
            return new RepositoryLinkEntity(new Uri(HttpUtility.UrlDecode(url)));
        }

        [HttpGet("{url}/commits")]
        public async Task<IEnumerable<CommitEntity>> FetchCommits(string url)
        {
            return await _fetchCommits.Fetch(ParseUrl(url));
        }

        [HttpGet("{url}/commits/{branch}")]
        public async Task<IEnumerable<CommitEntity>> FetchCommitsByBranch(string url, string branch)
        {
            return await _fetchCommitsByBranch.Fetch(ParseUrl(url), HttpUtility.UrlDecode(branch));
        }

        [HttpGet("{url}/commit/{id}")]
        public async Task<CommitEntity> FetchCommit(string url, string id)
        {
            return await _fetchCommitById.Fetch(ParseUrl(url), id);
        }
    }
}