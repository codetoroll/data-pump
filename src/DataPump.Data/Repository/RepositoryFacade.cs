using System.Collections.Generic;
using System.Threading.Tasks;
using DataPump.Core.Entity;
using DataPump.Core.Gateway;
using DataPump.Core.Sharp;

namespace DataPump.Data.Repository
{
    public sealed class RepositoryFacade : IRepositoryGateway
    {
        private readonly IGitGateway _gitGateway;
        private readonly IBitBucketGateway _bitBucketGateway;
        private readonly IGithubGateway _githubGateway;
        private readonly IGitlabGateway _gitlabGateway;

        public RepositoryFacade(IGitGateway gitGateway, IBitBucketGateway bitBucketGateway,
            IGithubGateway gitHubGateway, IGitlabGateway gitlabGateway)
        {
            _gitGateway = gitGateway;
            _bitBucketGateway = bitBucketGateway;
            _githubGateway = gitHubGateway;
            _gitlabGateway = gitlabGateway;
        }

        public Task<IEnumerable<CommitEntity>> FetchCommits(RepositoryLinkEntity link)
        {
            link.ThrowIfNull();

            switch (link.Service)
            {
                case RepositoryLinkEntity.RepositoryService.BitBucket:
                    return _bitBucketGateway.FetchCommits(link);
                case RepositoryLinkEntity.RepositoryService.Gitlab:
                    return _gitlabGateway.FetchCommits(link);
                case RepositoryLinkEntity.RepositoryService.Github:
                    return _githubGateway.FetchCommits(link);
                default:
                    return _gitGateway.FetchCommits(link);
            }
        }

        public Task<IEnumerable<CommitEntity>> FetchCommits(RepositoryLinkEntity link, string branch)
        {
            link.ThrowIfNull();
            branch.ThrowIfNull();

            switch (link.Service)
            {
                case RepositoryLinkEntity.RepositoryService.BitBucket:
                    return _bitBucketGateway.FetchCommits(link, branch);
                case RepositoryLinkEntity.RepositoryService.Gitlab:
                    return _gitlabGateway.FetchCommits(link, branch);
                case RepositoryLinkEntity.RepositoryService.Github:
                    return _githubGateway.FetchCommits(link, branch);
                default:
                    return _gitGateway.FetchCommits(link, branch);
            }
        }

        public Task<IEnumerable<BranchEntity>> FetchBranches(RepositoryLinkEntity link)
        {
            link.ThrowIfNull();

            switch (link.Service)
            {
                case RepositoryLinkEntity.RepositoryService.BitBucket:
                    return _bitBucketGateway.FetchBranches(link);
                case RepositoryLinkEntity.RepositoryService.Gitlab:
                    return _gitlabGateway.FetchBranches(link);
                case RepositoryLinkEntity.RepositoryService.Github:
                    return _githubGateway.FetchBranches(link);
                default:
                    return _gitGateway.FetchBranches(link);
            }
        }

        public Task<CommitEntity> FetchCommit(RepositoryLinkEntity link, string id)
        {
            link.ThrowIfNull();
            id.ThrowIfNull();

            switch (link.Service)
            {
                case RepositoryLinkEntity.RepositoryService.BitBucket:
                    return _bitBucketGateway.FetchCommit(link, id);
                case RepositoryLinkEntity.RepositoryService.Gitlab:
                    return _gitlabGateway.FetchCommit(link, id);
                case RepositoryLinkEntity.RepositoryService.Github:
                    return _githubGateway.FetchCommit(link, id);
                default:
                    return _gitGateway.FetchCommit(link, id);
            }
        }
    }
}