namespace DataPump.Data.Repository.Gitlab
{
    public sealed class BranchResponseData
    {
        public string Name { get; set; }
    }
}