using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using DataPump.Core.Entity;
using DataPump.Core.Gateway;
using DataPump.Core.Sharp;
using DataPump.Data.RestSharp;
using Newtonsoft.Json;
using RestSharp;
using static DataPump.Core.Exception.RepositoryException;

namespace DataPump.Data.Repository.Gitlab
{
    public sealed class GitlabGateway : IGitlabGateway
    {
        private const string GitlabApiURL = "https://gitlab.com/api/v4/";
        private readonly RestClient _client = new RestClient(GitlabApiURL);

        private static string MakeId(RepositoryLinkEntity link)
        {
            return HttpUtility.UrlEncode($"{link.UserName}/{link.Slug}");
        }

        private static void ValidateLink(RepositoryLinkEntity link)
        {
            link.ThrowIfNull();

            if (link.Service != RepositoryLinkEntity.RepositoryService.Gitlab)
                throw new ArgumentException();
        }

        private async Task<TE> ProcessRequest<TE, TD>(string url, Func<TD, TE> mapFunction)
        {
            var request = new RestRequest(url)
            {
                JsonSerializer = new NewtonsoftJsonSerializer()
            };

            var response = await _client.ExecuteTaskAsync(request);

            if (!response.IsSuccessful)
            {
                if (response.StatusCode == HttpStatusCode.NotFound)
                    throw new NotExistsRepoException();

                throw new InvalidOperationException();
            }

            return mapFunction(JsonConvert.DeserializeObject<TD>(response.Content));
        }

        private readonly Func<CommitResponseData, CommitEntity> _commitMapper = item => new CommitEntity
        {
            Message = item.Message,
            Author = $"{item.AuthorName} {item.AuthorEmail}",
            Date = item.CreatedAt,
            Hash = item.Id
        };

        private readonly Func<BranchResponseData, BranchEntity> _branchMapper = item => new BranchEntity
        {
            Name = item.Name
        };


        public Task<IEnumerable<CommitEntity>> FetchCommits(RepositoryLinkEntity link)
        {
            ValidateLink(link);

            return ProcessRequest<IEnumerable<CommitEntity>, List<CommitResponseData>>(
                $"projects/{MakeId(link)}/repository/commits",
                data => data.Select(_commitMapper));
        }

        public Task<IEnumerable<CommitEntity>> FetchCommits(RepositoryLinkEntity link, string branch)
        {
            ValidateLink(link);
            branch.ThrowIfNull();

            return ProcessRequest<IEnumerable<CommitEntity>, List<CommitResponseData>>(
                $"projects/{MakeId(link)}/repository/commits?ref_name={branch}",
                data => data.Select(_commitMapper));
        }

        public Task<IEnumerable<BranchEntity>> FetchBranches(RepositoryLinkEntity link)
        {
            ValidateLink(link);

            return ProcessRequest<IEnumerable<BranchEntity>, List<BranchResponseData>>(
                $"projects/{MakeId(link)}/repository/branches",
                data => data.Select(_branchMapper));
        }

        public Task<CommitEntity> FetchCommit(RepositoryLinkEntity link, string id)
        {
            ValidateLink(link);
            id.ThrowIfNull();

            return ProcessRequest(
                $"projects/{MakeId(link)}/repository/commits/{id}",
                _commitMapper);
        }
    }
}