using System;
using Newtonsoft.Json;

namespace DataPump.Data.Repository.Gitlab
{
    public sealed class CommitResponseData
    {
        public string Id { get; set; }
        [JsonProperty("author_name")]
        public string AuthorName { get; set; }
        [JsonProperty("author_email")]
        public string AuthorEmail { get; set; }
        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }
        public string Message { get; set; }
    }
}