using System.Collections.Generic;

namespace DataPump.Data.Repository.BitBucket
{
    public sealed class BranchData
    {
        public string Name { get; set; }
    }

    public sealed class BranchesResponseData
    {
        public IList<BranchData> Values { get; set; }
    }
}