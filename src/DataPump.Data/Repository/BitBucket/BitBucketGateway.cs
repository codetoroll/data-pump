using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DataPump.Core.Entity;
using DataPump.Core.Gateway;
using DataPump.Core.Sharp;
using DataPump.Data.RestSharp;
using Newtonsoft.Json;
using RestSharp;
using static DataPump.Core.Exception.RepositoryException;

namespace DataPump.Data.Repository.BitBucket
{
    public sealed class BitBucketGateway : IBitBucketGateway
    {
        private const string BitBucketApiUrl = "https://api.bitbucket.org/2.0/";
        private readonly RestClient _client = new RestClient(BitBucketApiUrl);

        private static void ValidateLink(RepositoryLinkEntity link)
        {
            link.ThrowIfNull();

            if (link.Service != RepositoryLinkEntity.RepositoryService.BitBucket)
                throw new ArgumentException();
        }

        private async Task<TE> ProcessRequest<TE, TD>(string url, Func<TD, TE> mapFunction)
        {
            var request = new RestRequest(url)
            {
                JsonSerializer = new NewtonsoftJsonSerializer()
            };

            var response = await _client.ExecuteTaskAsync(request);

            if (!response.IsSuccessful)
            {
                if (response.StatusCode == HttpStatusCode.NotFound)
                    throw new NotExistsRepoException();

                throw new InvalidOperationException();
            }

            return mapFunction(JsonConvert.DeserializeObject<TD>(response.Content));
        }

        private readonly Func<CommitData, CommitEntity> _commitMapper = item => new CommitEntity
        {
            Message = item.Message,
            Author = item.Author.Raw,
            Date = item.Date,
            Hash = item.Hash
        };

        private readonly Func<BranchData, BranchEntity> _branchMapper = item => new BranchEntity
        {
            Name = item.Name
        };

        public Task<IEnumerable<CommitEntity>> FetchCommits(RepositoryLinkEntity link)
        {
            ValidateLink(link);
            return ProcessRequest<IEnumerable<CommitEntity>, CommitsResponseData>(
                $"repositories/{link.UserName}/{link.Slug}/commits/master",
                data => data.Values.Select(_commitMapper));
        }

        public Task<IEnumerable<CommitEntity>> FetchCommits(RepositoryLinkEntity link, string branch)
        {
            ValidateLink(link);
            branch.ThrowIfNull();
            return ProcessRequest<IEnumerable<CommitEntity>, CommitsResponseData>(
                $"repositories/{link.UserName}/{link.Slug}/commits/{branch}",
                data => data.Values.Select(_commitMapper));
        }

        public Task<IEnumerable<BranchEntity>> FetchBranches(RepositoryLinkEntity link)
        {
            ValidateLink(link);

            return ProcessRequest<IEnumerable<BranchEntity>, BranchesResponseData>(
                $"repositories/{link.UserName}/{link.Slug}/refs/branches",
                data => data.Values.Select(_branchMapper));
        }

        public Task<CommitEntity> FetchCommit(RepositoryLinkEntity link, string id)
        {
            ValidateLink(link);

            id.ThrowIfNull();

            return ProcessRequest(
                $"repositories/{link.UserName}/{link.Slug}/commit/{id}",
                _commitMapper);
        }
    }
}