using System;
using System.Collections.Generic;

namespace DataPump.Data.Repository.BitBucket
{
    public sealed class AuthorData
    {
        public string Raw { get; set; }
    }

    public sealed class CommitData
    {
        public string Hash { get; set; }
        public AuthorData Author { get; set; }
        public DateTime Date { get; set; }
        public string Message { get; set; }
    }

    public sealed class CommitsResponseData
    {
        public IList<CommitData> Values { get; set; }
    }
}