namespace DataPump.Data.Repository.Github
{
    public sealed class BranchResponseData
    {
        public string Name { get; set; }
    }
}