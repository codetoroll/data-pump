using System;

namespace DataPump.Data.Repository.Github
{
    public sealed class CommitData
    {
        public AuthorData Author { get; set; }
        public string Message { get; set; }
    }

    public sealed class AuthorData
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public DateTime Date { get; set; }
    }

    public sealed class CommitResponseData
    {
        public string SHA { get; set; }
        public CommitData Commit { get; set; }
    }
}