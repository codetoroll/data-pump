using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DataPump.Core;
using DataPump.Core.Entity;
using static DataPump.Core.Exception.RepositoryException;
using DataPump.Core.Gateway;
using DataPump.Core.Sharp;
using DataPump.Data.RestSharp;
using Newtonsoft.Json;
using RestSharp;

namespace DataPump.Data.Repository.Github
{
    public sealed class GithubGateway : IGithubGateway
    {
        private string TOKEN = Environment.GetEnvironmentVariable(Constants.GithubApiKey);
        private const string GithubApiURL = "https://api.github.com/";
        private readonly RestClient _client = new RestClient(GithubApiURL);

        private void InjectToken(IRestRequest request)
        {
            if (!string.IsNullOrEmpty(TOKEN))
            {
                request.AddHeader("Authorization", $"token {TOKEN}");
            }
        }

        private static void ValidateLink(RepositoryLinkEntity link)
        {
            link.ThrowIfNull();

            if (link.Service != RepositoryLinkEntity.RepositoryService.Github)
                throw new ArgumentException();
        }

        private async Task<TE> ProcessRequest<TE, TD>(string url, Func<TD, TE> mapFunction)
        {
            var request = new RestRequest(url)
            {
                JsonSerializer = new NewtonsoftJsonSerializer()
            };

            InjectToken(request);

            var response = await _client.ExecuteTaskAsync(request);

            if (!response.IsSuccessful)
            {
                if (response.StatusCode == HttpStatusCode.NotFound)
                    throw new NotExistsRepoException();

                throw new InvalidOperationException();
            }

            return mapFunction(JsonConvert.DeserializeObject<TD>(response.Content));
        }

        private readonly Func<CommitResponseData, CommitEntity> _commitMapper = item => new CommitEntity
        {
            Message = item.Commit.Message,
            Author = $"{item.Commit.Author.Name} {item.Commit.Author.Email}",
            Date = item.Commit.Author.Date,
            Hash = item.SHA
        };

        private readonly Func<BranchResponseData, BranchEntity> _branchMapper = item => new BranchEntity
        {
            Name = item.Name
        };

        public Task<IEnumerable<CommitEntity>> FetchCommits(RepositoryLinkEntity link)
        {
            ValidateLink(link);
            return ProcessRequest<IEnumerable<CommitEntity>, List<CommitResponseData>>(
                $"repos/{link.UserName}/{link.Slug}/commits", data => data.Select(_commitMapper));
        }

        public Task<IEnumerable<CommitEntity>> FetchCommits(RepositoryLinkEntity link, string branch)
        {
            ValidateLink(link);
            branch.ThrowIfNull();
            return ProcessRequest<IEnumerable<CommitEntity>, List<CommitResponseData>>(
                $"repos/{link.UserName}/{link.Slug}/commits?sha={branch}", data => data.Select(_commitMapper));
        }

        public Task<IEnumerable<BranchEntity>> FetchBranches(RepositoryLinkEntity link)
        {
            ValidateLink(link);
            return ProcessRequest<IEnumerable<BranchEntity>, List<BranchResponseData>>(
                $"repos/{link.UserName}/{link.Slug}/branches", data => data.Select(_branchMapper));
        }

        public Task<CommitEntity> FetchCommit(RepositoryLinkEntity link, string id)
        {
            ValidateLink(link);
            id.ThrowIfNull();

            return ProcessRequest($"repos/{link.UserName}/{link.Slug}/commits/{id}",
                _commitMapper);
        }
    }
}