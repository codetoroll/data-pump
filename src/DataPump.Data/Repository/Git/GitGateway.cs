using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataPump.Core;
using DataPump.Core.Entity;
using DataPump.Core.Gateway;
using LibGit2Sharp;

namespace DataPump.Data.Repository.Git
{
    public sealed class GitGateway : IGitGateway
    {
        private readonly Dictionary<string, string> _paths = new Dictionary<string, string>();

        private static CommitEntity ToEntity(Commit commit, IEnumerable<ChangeEntity> changes)
        {
            if (commit == null)
                return null;

            return new CommitEntity
            {
                Author = $"{commit.Author.Email} {commit.Author.Name}",
                Message = commit.Message,
                Date = commit.Committer.When.Date,
                Hash = commit.Sha,
                Changes = changes
            };
        }

        private string GetRepositoryPath(RepositoryLinkEntity link)
        {
            var url = link.Uri.ToString();

            if (_paths.ContainsKey(url))
            {
                return _paths[url];
            }

            var path = LibGit2Sharp.Repository.Clone(url, Constants.RepoPath);
            _paths[url] = path;
            return path;
        }

        public async Task<IEnumerable<CommitEntity>> FetchCommits(RepositoryLinkEntity link)
        {
            if (link == null)
                throw new ArgumentNullException();

            var commits = new List<CommitEntity>();
            using (var repo = new LibGit2Sharp.Repository(GetRepositoryPath(link)))
            {
                commits.AddRange(from commit in repo.Commits
                    let changes = (from parent in commit.Parents
                        from change in repo.Diff.Compare<TreeChanges>(parent.Tree, commit.Tree)
                        select new ChangeEntity {FileName = change.Path}).ToList()
                    select ToEntity(commit, changes));
            }

            return commits;
        }

        public async Task<IEnumerable<CommitEntity>> FetchCommits(RepositoryLinkEntity link, string branch)
        {
            if (link == null || branch == null)
                throw new ArgumentNullException();

            var commits = new List<CommitEntity>();
            using (var repo = new LibGit2Sharp.Repository(GetRepositoryPath(link)))
            {
                var repoBranch = repo.Branches[$"origin/{branch}"];
                //TODO Check branch nullability

                commits.AddRange(from commit in repoBranch.Commits
                    let changes = (from parent in commit.Parents
                        from change in repo.Diff.Compare<TreeChanges>(parent.Tree, commit.Tree)
                        select new ChangeEntity {FileName = change.Path}).ToList()
                    select ToEntity(commit, changes));
            }

            return commits;
        }

        public async Task<CommitEntity> FetchCommit(RepositoryLinkEntity link, string id)
        {
            if (link == null || id == null)
                throw new ArgumentNullException();

            CommitEntity commitEntity;
            using (var repo = new LibGit2Sharp.Repository(GetRepositoryPath(link)))
            {
                var commit = repo.Lookup<Commit>(id);
                //TODO Check commit nullability

                var changes = (from parent in commit.Parents
                    from change in repo.Diff.Compare<TreeChanges>(parent.Tree, commit.Tree)
                    select new ChangeEntity {FileName = change.Path}).ToList();

                commitEntity = ToEntity(commit, changes);
            }

            //TODO throw specific login exception
            if (commitEntity == null)
                throw new Exception();

            return commitEntity;
        }

        public async Task<IEnumerable<BranchEntity>> FetchBranches(RepositoryLinkEntity link)
        {
            if (link == null)
                throw new ArgumentNullException();

            var branches = new List<BranchEntity>();
            using (var repo = new LibGit2Sharp.Repository(GetRepositoryPath(link)))
            {
                branches.AddRange(repo.Branches.Where(b => b.IsRemote)
                    .Select(branch => new BranchEntity
                        {Name = branch.FriendlyName.TrimStart("origin/".ToCharArray())}));
            }

            return branches;
        }
    }
}