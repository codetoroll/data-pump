using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataPump.Core;
using DataPump.Core.Entity;
using DataPump.Core.Gateway;
using DataPump.Core.Sharp;
using ProGaudi.Tarantool.Client;

namespace DataPump.Data.Database
{
    public sealed class TarantoolBranchesDatabase : IBranchesDatabase
    {
        private readonly string _connectionString =
            Environment.GetEnvironmentVariable(Constants.TarantoolConnectionStringKey);

        private const string BranchesSpace = "Branches";

        public async Task Store(RepositoryLinkEntity link, IEnumerable<BranchEntity> branches)
        {
            link.ThrowIfNull();
            branches.ThrowIfNull();

            using (var client = await Box.Connect(_connectionString))
            {
                var schema = client.GetSchema();
                var space = schema[BranchesSpace];

                foreach (var branchEntity in branches)
                {
                    await space.Insert((link.Service.ToString(), link.Service.ToString(), branchEntity.Name));
                }
            }
        }
    }
}