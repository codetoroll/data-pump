namespace DataPump.Core
{
    public static class Constants
    {
        public const string RepoPath = "./repos";
        public const string GithubApiKey = "GITHUB_KEY";
        public const string TarantoolConnectionStringKey = "TARANTOOL_CONNECTION_STRING";
    }
}