using System.Collections.Generic;
using System.Threading.Tasks;
using DataPump.Core.Entity;
using static DataPump.Core.Exception.FetchBranchesListException;
using JetBrains.Annotations;

namespace DataPump.Core.UseCase
{
    public interface IFetchBranchesListUseCase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="link"></param>
        /// <exception cref="NotExistsRepoException"></exception>
        /// <returns></returns>
        [NotNull]
        Task<IEnumerable<BranchEntity>> Fetch([NotNull] RepositoryLinkEntity link);
    }
}