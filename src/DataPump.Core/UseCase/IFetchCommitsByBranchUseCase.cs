using System.Collections.Generic;
using System.Threading.Tasks;
using DataPump.Core.Entity;
using JetBrains.Annotations;

namespace DataPump.Core.UseCase
{
    public interface IFetchCommitsByBranchUseCase
    {
        [NotNull]
        Task<IEnumerable<CommitEntity>> Fetch([NotNull] RepositoryLinkEntity link, [NotNull] string branch);
    }
}