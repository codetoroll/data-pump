using System.Collections.Generic;
using System.Threading.Tasks;
using DataPump.Core.Entity;
using JetBrains.Annotations;

namespace DataPump.Core.Gateway
{
    public interface IRepositoryGateway
    {
        [NotNull]
        Task<IEnumerable<CommitEntity>> FetchCommits([NotNull] RepositoryLinkEntity link);

        [NotNull]
        Task<IEnumerable<CommitEntity>> FetchCommits([NotNull] RepositoryLinkEntity link, [NotNull] string branch);

        [NotNull]
        Task<IEnumerable<BranchEntity>> FetchBranches([NotNull] RepositoryLinkEntity link);

        [NotNull]
        Task<CommitEntity> FetchCommit([NotNull] RepositoryLinkEntity link, [NotNull] string id);
    }
}