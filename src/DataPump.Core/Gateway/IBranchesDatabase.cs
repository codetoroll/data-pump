using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataPump.Core.Entity;

namespace DataPump.Core.Gateway
{
    public interface IBranchesDatabase
    {
        Task Store(RepositoryLinkEntity link, IEnumerable<BranchEntity> branches);
    }
}