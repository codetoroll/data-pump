using System;

namespace DataPump.Core.Exception
{
    [Serializable]
    public abstract class RepositoryException : System.Exception
    {
        #region Constructors

        protected RepositoryException() : base()
        {
        }

        protected RepositoryException(string message) : base(message)
        {
        }

        protected RepositoryException(string message, System.Exception inner) : base(message, inner)
        {
        }

        #endregion

        #region GatewayExceptions

        [Serializable]
        public class NotExistsRepoException : RepositoryException
        {
            public NotExistsRepoException() : base()
            {
            }

            public NotExistsRepoException(string message) : base(message)
            {
            }

            public NotExistsRepoException(string message, System.Exception inner) : base(message, inner)
            {
            }
        }

        #endregion
    }
}