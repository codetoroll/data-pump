using System;

namespace DataPump.Core.Exception
{
    [Serializable]
    public abstract class FetchBranchesListException : System.Exception
    {
        #region Constructors

        private FetchBranchesListException() : base()
        {
        }

        private FetchBranchesListException(string message) : base(message)
        {
        }

        private FetchBranchesListException(string message, System.Exception inner) : base(message, inner)
        {
        }

        #endregion

        #region UseCaseExceptions

        [Serializable]
        public class NotExistsRepoException : FetchBranchesListException
        {
            public NotExistsRepoException() : base()
            {
            }

            public NotExistsRepoException(string message) : base(message)
            {
            }

            public NotExistsRepoException(string message, System.Exception inner) : base(message, inner)
            {
            }
        }

        #endregion
    }
}