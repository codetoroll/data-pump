using System;
using JetBrains.Annotations;

namespace DataPump.Core.Sharp
{
    public static class Intrinsics
    {
        [NotNull]
        public static T ThrowIfNull<T>(this T assertObject) where T : class
        {
            if (assertObject == null)
                throw new ArgumentNullException(nameof(assertObject));
            return assertObject;
        }
    }
}