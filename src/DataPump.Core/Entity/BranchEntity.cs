using JetBrains.Annotations;

namespace DataPump.Core.Entity
{
    public sealed class BranchEntity
    {
     public string Name { get; set; }
    }
}