using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace DataPump.Core.Entity
{
    public sealed class CommitEntity
    {
        public string Message { get; set; }
         public string Author { get; set; }
        public DateTime Date { get; set; }
       public string Hash { get; set; }
       public IEnumerable<ChangeEntity> Changes { get; set; }

        private sealed class CommitEntityEqualityComparer : IEqualityComparer<CommitEntity>
        {
            public bool Equals(CommitEntity x, CommitEntity y)
            {
                if (ReferenceEquals(x, y)) return true;
                if (ReferenceEquals(x, null)) return false;
                if (ReferenceEquals(y, null)) return false;
                if (x.GetType() != y.GetType()) return false;
                return string.Equals(x.Message, y.Message) && string.Equals(x.Author, y.Author) &&
                       x.Date.Equals(y.Date) && string.Equals(x.Hash, y.Hash) && Equals(x.Changes, y.Changes);
            }

            public int GetHashCode(CommitEntity obj)
            {
                unchecked
                {
                    var hashCode = obj.Message.GetHashCode();
                    hashCode = (hashCode * 397) ^ obj.Author.GetHashCode();
                    hashCode = (hashCode * 397) ^ obj.Date.GetHashCode();
                    hashCode = (hashCode * 397) ^ obj.Hash.GetHashCode();
                    hashCode = (hashCode * 397) ^ (obj.Changes != null ? obj.Changes.GetHashCode() : 0);
                    return hashCode;
                }
            }
        }

        public static IEqualityComparer<CommitEntity> CommitEntityComparer { get; } =
            new CommitEntityEqualityComparer();
    }
}