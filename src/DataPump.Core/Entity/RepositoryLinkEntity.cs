using System;
using JetBrains.Annotations;

namespace DataPump.Core.Entity
{
    public sealed class RepositoryLinkEntity
    {
        public RepositoryLinkEntity([NotNull] string uri) : this(new Uri(uri))
        {
        }

        public RepositoryLinkEntity([NotNull] Uri uri)
        {
            if (uri == null)
                throw new ArgumentNullException();

            Uri = uri;
            Protocol = ParseProtocol(uri.Scheme);
            Service = ParseService(uri.Host);
            Type = uri.AbsolutePath.EndsWith(".git") || Service == RepositoryService.Gitlab
                ? RepositoryType.Git
                : RepositoryType.Undefined;
        }

        private static LinkProtocol ParseProtocol(string scheme)
        {
            switch (scheme)
            {
                case "http":
                    return LinkProtocol.Http;
                case "https":
                    return LinkProtocol.Https;
                default:
                    return LinkProtocol.Unexpected;
            }
        }

        public enum LinkProtocol
        {
            Http,
            Https,
            Unexpected
        }

        public enum RepositoryType
        {
            Git,
            Subversion,
            Undefined
        }

        private static RepositoryService ParseService(string host)
        {
            switch (host)
            {
                case "bitbucket.org":
                    return RepositoryService.BitBucket;
                case "gitlab.com":
                    return RepositoryService.Gitlab;
                case "github.com":
                    return RepositoryService.Github;
                default:
                    return RepositoryService.Undefined;
            }
        }

        public enum RepositoryService
        {
            Github,
            Gitlab,
            BitBucket,
            Undefined
        }

        public LinkProtocol Protocol { get; }
        public RepositoryType Type { get; }
        public RepositoryService Service { get; }
         public Uri Uri { get; }

        [CanBeNull]
        public string UserName
        {
            get
            {
                var parts = Uri.Segments;
                return parts.Length < 2 ? null : parts[1].TrimEnd('/');
            }
        }

        [CanBeNull]
        public string Slug
        {
            get
            {
                var parts = Uri.Segments;
                return parts.Length < 3 ? null : parts[2].TrimEnd('/');
            }
        }

        [CanBeNull]
        public string IdString => string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Slug)
            ? null
            : $"{Type}:{UserName}:{Slug}";
    }
}